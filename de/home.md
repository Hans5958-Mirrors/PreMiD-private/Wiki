---
title: Startseite
description: Offizielle Dokumentation für v2
published: true
date: 2019-10-03T11:54:02.313Z
tags: 
---

> :warning: Die Dokumentation wird stetig bearbeitet! Einige Seiten sind möglicherweise unvollständig oder fehlen.
{.is-warning}

# Über PreMiD
- :mag: [What is PreMiD?](/about) Absichten hinter PreMiD und warum Du es verwenden sollten.
- :link: [What is RPC?](https://discordapp.com/rich-presence) Alles über Rich Presence und ihre Funktionen.

# Getting Started

PreMiD ist schnell und einfach zu installieren. Du solltest in kürzester Zeit einsatzbereit sein.

- Stelle sicher, dass Du die [Systemanforderungen](/install/requirements) gelesen hast.
- Befolge die schnelle und einfache [Installationsanleitung](/install).
- Die [Fehlerbehebung](/troubleshooting) führt Dich durch Lösungen für häufig auftretende Probleme.

# Developers

[Getting Started](/dev) for PreMiD development is your first step before coding some things.

- :computer: [API](/dev/api): Access ressources and perform actions using the PreMiD API.
- :wrench: [Presence Development](/dev/presence): Learn how to use our Presence system.

# Contribute
- :bug: [Report a Bug](https://github.com/PreMiD): Help us squash those pesky bugs.
- :bulb: [Suggest a New Feature](https://discord.gg/premid): We need your ideas!
- :heart: [Donate](https://www.patreon.com/Timeraa): Make a small donation or become a sponsor of this wonderful project!
- :globe_with_meridians: [Locales](https://translate.premid.app): Let other users use PreMiD in their native language.

![](https://beta.premid.app/img/logo.2b414dc2.gif){.align-abstopright}